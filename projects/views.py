from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import new_project_form
from django.contrib.auth.decorators import login_required


@login_required
def Projects_list(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = new_project_form(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = new_project_form()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)


# Create your views here.
