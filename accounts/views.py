from django.contrib.auth import authenticate, login, logout
from accounts.forms import Login, SignUp
from django.shortcuts import render, redirect
from django.contrib.auth.models import User


def show_login(request):
    if request.method == "POST":
        form = Login(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = Login()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def show_logout(request):
    logout(request)
    return redirect("login")


def show_signup(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_conf = form.cleaned_data["password_confirmation"]
            if password == password_conf:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password_conf", "the passwords do not match")
    else:
        form = SignUp()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
