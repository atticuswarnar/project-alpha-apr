from django.urls import path
from accounts.views import show_login, show_logout, show_signup


urlpatterns = [
    path("signup/", show_signup, name="signup"),
    path("logout/", show_logout, name="logout"),
    path("login/", show_login, name="login"),
]
